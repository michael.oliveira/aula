<html>
    <head>
        <meta charset="UTF-8">
        <title></title>
    </head>
    <body>
        <?php 
        $con = new PDO("mysql:host=localhost;dbname=venda", "root", "Fms237691");
        $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        
        $id = $_GET['id'];
        
        $sql = "SELECT * FROM cliente WHERE cod_cliente = :id";
        $stm = $con->prepare($sql); 
        $stm->bindValue("id", $id);
        $stm->execute();
        $cliente = $stm->fetch(PDO::FETCH_ASSOC);
        ?>
        <form method="post" action="recebe.php">
            <label>Nome</label>
            <input name="nome" type="text" class="form-control"
                   value="<?= $cliente['nome']; ?>"
                   />
            <label>RG</label>
            <input name="rg" type="text" class="form-control"
                   value="<?= $cliente['rg']; ?>"
                   />
            <input class="btn btn-primary" type="submit" value="Salvar" />
        </form>
    </body>
</html>
