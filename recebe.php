<?php
extract($_POST);
try{
    $con = new PDO("mysql:host=localhost;dbname=venda", "root", "alunocepa");
    $con->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
    $sql = "INSERT INTO cliente(nome, rg) VALUES(:nome, :rg)";
    $stm = $con->prepare($sql);
    $stm->bindValue("nome", $nome);
    $stm->bindValue("rg", $rg);

    $stm->execute();

    echo "Cadastro realizado com Sucesso!";
} catch (PDOException $e) {
    echo "Error: " . $e->getMessage();
}
